﻿namespace PixelStacker.UI
{
    partial class OtherOptionsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OtherOptionsWindow));
            this.nbrGridSize = new System.Windows.Forms.NumericUpDown();
            this.nbrMaxHeight = new System.Windows.Forms.NumericUpDown();
            this.nbrMaxWidth = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxIsSideView = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.nbrGridSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbrMaxHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbrMaxWidth)).BeginInit();
            this.SuspendLayout();
            // 
            // nbrGridSize
            // 
            this.nbrGridSize.Location = new System.Drawing.Point(111, 12);
            this.nbrGridSize.Name = "nbrGridSize";
            this.nbrGridSize.Size = new System.Drawing.Size(112, 20);
            this.nbrGridSize.TabIndex = 0;
            this.nbrGridSize.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.nbrGridSize.ValueChanged += new System.EventHandler(this.nbrGridSize_ValueChanged);
            // 
            // nbrMaxHeight
            // 
            this.nbrMaxHeight.Location = new System.Drawing.Point(111, 72);
            this.nbrMaxHeight.Name = "nbrMaxHeight";
            this.nbrMaxHeight.Size = new System.Drawing.Size(112, 20);
            this.nbrMaxHeight.TabIndex = 1;
            this.nbrMaxHeight.ValueChanged += new System.EventHandler(this.nbrMaxHeight_ValueChanged);
            // 
            // nbrMaxWidth
            // 
            this.nbrMaxWidth.Location = new System.Drawing.Point(111, 98);
            this.nbrMaxWidth.Name = "nbrMaxWidth";
            this.nbrMaxWidth.Size = new System.Drawing.Size(112, 20);
            this.nbrMaxWidth.TabIndex = 2;
            this.nbrMaxWidth.ValueChanged += new System.EventHandler(this.nbrMaxWidth_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Grid Size";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Max Height";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Max Width";
            // 
            // cbxIsSideView
            // 
            this.cbxIsSideView.AutoSize = true;
            this.cbxIsSideView.Location = new System.Drawing.Point(12, 124);
            this.cbxIsSideView.Name = "cbxIsSideView";
            this.cbxIsSideView.Size = new System.Drawing.Size(206, 17);
            this.cbxIsSideView.TabIndex = 8;
            this.cbxIsSideView.Text = "Side View? (Affects output orientation)";
            this.cbxIsSideView.UseVisualStyleBackColor = true;
            this.cbxIsSideView.CheckedChanged += new System.EventHandler(this.cbxIsSideView_CheckedChanged);
            // 
            // OtherOptionsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(239, 339);
            this.Controls.Add(this.cbxIsSideView);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nbrMaxWidth);
            this.Controls.Add(this.nbrMaxHeight);
            this.Controls.Add(this.nbrGridSize);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OtherOptionsWindow";
            this.Text = "Other Options";
            ((System.ComponentModel.ISupportInitialize)(this.nbrGridSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbrMaxHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbrMaxWidth)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nbrGridSize;
        private System.Windows.Forms.NumericUpDown nbrMaxHeight;
        private System.Windows.Forms.NumericUpDown nbrMaxWidth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbxIsSideView;
    }
}