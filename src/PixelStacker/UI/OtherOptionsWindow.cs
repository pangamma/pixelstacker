﻿using PixelStacker.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PixelStacker.UI
{
    public partial class OtherOptionsWindow : Form
    {
        private bool suspendOptionsSave = false;
        public OtherOptionsWindow()
        {
            suspendOptionsSave = true;
            InitializeComponent();
            this.cbxIsSideView.Checked = Options.Get.IsSideView;
            this.nbrGridSize.Value = Options.Get.GridSize;
            this.nbrMaxHeight.Maximum = Options.Get.IsSideView ? 256 : short.MaxValue;
            this.nbrMaxHeight.Value = Options.Get.MaxHeight ?? 0;
            this.nbrMaxHeight.Text = nbrMaxHeight.Value.ToString();
            this.nbrMaxWidth.Value = Options.Get.MaxWidth ?? 0;
            suspendOptionsSave = false;
        }

        private void cbxIsSideView_CheckedChanged(object sender, EventArgs e)
        {
            if (suspendOptionsSave) return;
            Options.Get.IsSideView = cbxIsSideView.Checked;

            if (cbxIsSideView.Checked)
            {
                nbrMaxHeight.Value = Math.Min(256, Math.Max(0, Options.Get.MaxHeight ?? 0));
                nbrMaxWidth.Value = Math.Max(0, Options.Get.MaxWidth ?? 0);
                nbrMaxHeight.Maximum = 256;
            }
            else
            {
                nbrMaxHeight.Maximum = short.MaxValue;
                nbrMaxWidth.Maximum = short.MaxValue;
                nbrMaxHeight.Value = Math.Max(0, Options.Get.MaxHeight ?? 0);
                nbrMaxWidth.Value = Math.Max(0, Options.Get.MaxWidth ?? 0);
            }

            Options.Save();
            MainForm.Self.PreRenderedImage.DisposeSafely();
            MainForm.Self.PreRenderedImage = null;
        }

        private void nbrMaxWidth_ValueChanged(object sender, EventArgs e)
        {
            if (suspendOptionsSave) return;
            int i = Convert.ToInt32(nbrMaxWidth.Value);
            int? val = i > 0 ? i : default(int?);
            Options.Get.MaxWidth = val;
            Options.Save();
            MainForm.Self.PreRenderedImage.DisposeSafely();
            MainForm.Self.PreRenderedImage = null;
        }

        private void nbrMaxHeight_ValueChanged(object sender, EventArgs e)
        {
            if (suspendOptionsSave) return;
            int i = Convert.ToInt32(nbrMaxHeight.Value);
            int? val = i > 0 ? i : default(int?);
            Options.Get.MaxHeight = val;
            Options.Save();
            MainForm.Self.PreRenderedImage.DisposeSafely();
            MainForm.Self.PreRenderedImage = null;
        }

        private void nbrGridSize_ValueChanged(object sender, EventArgs e)
        {
            if (suspendOptionsSave) return;
            int i = Convert.ToInt32(nbrGridSize.Value);
            Options.Get.GridSize = i;
            Options.Save();
        }
    }
}
