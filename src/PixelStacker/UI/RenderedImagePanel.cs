﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PixelStacker.Logic;
using PixelStacker.Properties;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using PixelStacker.Logic.WIP;
using System.Threading;

namespace PixelStacker.UI
{
    public partial class RenderedImagePanel : UserControl
    {
        public int xOrigin = 0;
        public int yOrigin = 0;
        private BlueprintPA image;
        private Bitmap renderedImage;
        private int CalculatedTextureSize { get; set; } = Constants.TextureSize;
        
        public bool IsSolidColors { get { return Options.Get.IsEnabled("rendered_IsSolidColors", false); } set { Options.Get.SetEnabled("rendered_IsSolidColors", value); this.renderedImage.DisposeSafely(); this.renderedImage = null; if (value) { this.IsColorPalette = false; } } }
        public bool IsShowBorder { get { return Options.Get.IsEnabled("rendered_IsShowBorder", false); } set { Options.Get.SetEnabled("rendered_IsShowBorder", value); } }
        public bool IsShowGrid { get { return Options.Get.IsEnabled("rendered_IsShowGrid", false); } set { Options.Get.SetEnabled("rendered_IsShowGrid", value); } }
        public bool IsColorPalette { get { return Options.Get.IsEnabled("rendered_IsColorPalette", false); } set { Options.Get.SetEnabled("rendered_IsColorPalette", value); this.renderedImage.DisposeSafely(); this.renderedImage = null; if (value) { this.IsSolidColors = false; } } }


        private Point initialDragPoint;
        private bool IsDragging = false;

        public RenderedImagePanel()
        {
            InitializeComponent();
        }

        public void ForceReRender()
        {
            this.renderedImage = null;
            this.Refresh();
        }

        public void SetBluePrint(BlueprintPA src)
        {
            this.image = src;

            #region Calculate size
            {
                // Calculate texture size so we can handle large images.
                if (image == null) CalculatedTextureSize = Constants.TextureSize;
                int mbSize = (image.Width * image.Height * 32 / 8); // Still need to multiply by texture size
                int maxSize = 400 * 1024 * 1024; // max size in bytes we'll want to allow.
                int tSize = 16;
                if (mbSize * tSize * tSize-- <= maxSize) CalculatedTextureSize = tSize + 1; // 16
                else if (mbSize * tSize * tSize-- <= maxSize) CalculatedTextureSize = tSize + 1;
                else if (mbSize * tSize * tSize-- <= maxSize) CalculatedTextureSize = tSize + 1; // 14
                else if (mbSize * tSize * tSize-- <= maxSize) CalculatedTextureSize = tSize + 1;
                else if (mbSize * tSize * tSize-- <= maxSize) CalculatedTextureSize = tSize + 1;
                else if (mbSize * tSize * tSize-- <= maxSize) CalculatedTextureSize = tSize + 1;
                else if (mbSize * tSize * tSize-- <= maxSize) CalculatedTextureSize = tSize + 1; // 10
                else if (mbSize * tSize * tSize-- <= maxSize) CalculatedTextureSize = tSize + 1;
                else if (mbSize * tSize * tSize-- <= maxSize) CalculatedTextureSize = tSize + 1;
                else if (mbSize * tSize * tSize-- <= maxSize) CalculatedTextureSize = tSize + 1;
                else if (mbSize * tSize * tSize-- <= maxSize) CalculatedTextureSize = tSize + 1; // 6
                else if (mbSize * tSize * tSize-- <= maxSize) CalculatedTextureSize = tSize + 1;
                else CalculatedTextureSize = 4;

                bool isSuccess = false;
                do
                {
                    try
                    {
                        int numMegaBytes = 32 / 8 * CalculatedTextureSize * CalculatedTextureSize * image.Height * image.Width / 1024 / 1024;
                        using (var memoryCheck = new System.Runtime.MemoryFailPoint(numMegaBytes))
                        {
                        }

                        isSuccess = true;
                    }
                    catch (InsufficientMemoryException ex)
                    {
                        CalculatedTextureSize = Math.Max(1, CalculatedTextureSize - 2);
                    }
                } while (isSuccess == false && CalculatedTextureSize > 1);

                if (isSuccess == false)
                {
                    MessageBox.Show("Not enough memory. Please try again or reduce image size if problem continues.");
                    MainForm.Self.PreRenderedImage.DisposeSafely();
                    MainForm.Self.PreRenderedImage = null;
                    return;
                }
            }
            #endregion

            bool preserveZoom = (MainForm.PanZoomSettings != null);
            if (!preserveZoom)
            {
                var settings = new PanZoomSettings()
                {
                    initialImageX = 0,
                    initialImageY = 0,
                    imageX = 0,
                    imageY = 0,
                    zoomLevel = 0,
                    maxZoomLevel = 50.0D,
                    minZoomLevel = 0.0D,
                };

                bool isv = true;
                int mWidth = src.Mapper.GetXLength(isv);
                int mHeight = isv ? src.Mapper.GetYLength(isv) : src.Mapper.GetZLength(isv);

                double wRatio = (double)Width / mWidth;
                double hRatio = (double)Height / mHeight;
                if (hRatio < wRatio)
                {
                    settings.zoomLevel = hRatio;
                    settings.imageX = (Width - (int)(mWidth * hRatio)) / 2;
                }
                else
                {
                    settings.zoomLevel = wRatio;
                    settings.imageY = (Height - (int)(mHeight * wRatio)) / 2;
                }

                int numICareAbout = Math.Max(mWidth, mHeight);
                settings.minZoomLevel = (100.0D / numICareAbout);
                if (settings.minZoomLevel > 1.0D)
                {
                    settings.minZoomLevel = 1.0D;
                }

                MainForm.PanZoomSettings = settings;
            }

            renderedImage.DisposeSafely();
            renderedImage = null;
            Refresh();

            //var rndrWorker = BackgroundWorkerHelper.CreateWorker("RenderedImagePanel_RenderBitmap", 
            //    (object sender, DoWorkEventArgs workArgs) => {
            //        var worker = sender as BackgroundWorker;
            //        RenderBitmap(worker);
            //    }, (object sender, RunWorkerCompletedEventArgs args) => {
            //        Refresh();
            //    });

        }

        public void SaveToPNG(string filename)
        {
            if (this.renderedImage == null || this.image == null)
            {
                return;
            }

            bool isCompact = this.IsSolidColors && !this.IsShowGrid && !this.IsColorPalette;
            int blockWidth = isCompact ? 1 : CalculatedTextureSize;
            using (Bitmap bm = new Bitmap(this.image.Mapper.GetXLength(true) * blockWidth, this.image.Mapper.GetYLength(true) * blockWidth, PixelFormat.Format32bppArgb))
            {
                using (Graphics g = Graphics.FromImage(bm))
                {
                    g.InterpolationMode = InterpolationMode.NearestNeighbor;
                    g.SmoothingMode = SmoothingMode.None;
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;

                    bool isSide = Options.Get.IsSideView;

                    using (SolidBrush brush = new SolidBrush(Color.Black))
                    {
                        using (Pen pen = new Pen(brush))
                        {
                            g.DrawImage(image: this.renderedImage,
                                x: 0, y: 0,
                                width: bm.Width,
                                height: bm.Height);


                            if (this.IsShowBorder)
                            {
                                int indexShift = isCompact ? 1 : 0;
                                Point pp2 = new Point(indexShift, indexShift);
                                using (Pen penWE = new Pen(Color.FromArgb(127, 0, 0, 0), blockWidth))
                                {
                                    penWE.Alignment = PenAlignment.Inset;
                                    g.DrawRectangle(penWE, pp2.X, pp2.Y, (int)(bm.Width) - indexShift, (int)(bm.Height) - indexShift);
                                }
                            }

                            if (Constants.IsFullVersion)
                            {
                                brush.Color = Color.Red;
                                Point wePoint = getPointOnPanel(image.WorldEditOrigin);
                                g.FillRectangle(brush, image.WorldEditOrigin.X * blockWidth, image.WorldEditOrigin.Y * blockWidth, blockWidth, blockWidth);
                            }

                            if ((this.IsShowGrid) /* && (this.zoomLevel > 0.3D)*/)
                            {
                                int gridSize = Options.Get.GridSize;
                                int numHorizBlocks = (this.image.Width / gridSize);
                                int numVertBlocks = (this.image.Height / gridSize);
                                using (Pen p = new Pen(Color.Black, 2))
                                {
                                    p.Alignment = PenAlignment.Inset;

                                    g.DrawLine(p, 0, 0, bm.Width, 0);
                                    g.DrawLine(p, 0, bm.Height, bm.Width, bm.Height);
                                    g.DrawLine(p, 0, 0, 0, bm.Height);
                                    g.DrawLine(p, bm.Width, 0, bm.Width, bm.Height);

                                    for (int x = 0; x < numHorizBlocks; x++)
                                    {
                                        g.DrawLine(p, x * gridSize * CalculatedTextureSize, 0, x * gridSize * CalculatedTextureSize, bm.Height * CalculatedTextureSize);
                                    }

                                    for (int y = 0; y < numVertBlocks; y++)
                                    {
                                        g.DrawLine(p, 0, y * gridSize * CalculatedTextureSize, bm.Width, y * gridSize * CalculatedTextureSize);
                                    }
                                }

                                using (Pen p = new Pen(Color.FromArgb(40, 0, 0, 0), 2))
                                {
                                    p.Alignment = PenAlignment.Inset;

                                    for (int x = 0; x < bm.Width / CalculatedTextureSize; x++)
                                    {
                                        g.DrawLine(p, x * CalculatedTextureSize, 0, x * CalculatedTextureSize, bm.Height);
                                    }

                                    for (int y = 0; y < bm.Height / CalculatedTextureSize; y++)
                                    {
                                        g.DrawLine(p, 0, y * CalculatedTextureSize, bm.Width, y * CalculatedTextureSize);
                                    }
                                }
                            }
                        }
                    }
                }

                bm.Save(filename, ImageFormat.Png);
            }
        }


        public void RenderBitmap(BackgroundWorker worker)
        {
            //base.OnPaint(e);

            if (this.image != null)
            {
                bool isSelectiveLayerViewEnabled = Options.Get.IsEnabled(Constants.RenderedZIndexFilter, false);

                bool isSide = Options.Get.IsSideView;
                double origW = this.image.Width;
                double origH = this.image.Height;
                int w = (int)(origW * MainForm.PanZoomSettings.zoomLevel);
                int h = (int)(origH * MainForm.PanZoomSettings.zoomLevel);
                int zoom = (int)(MainForm.PanZoomSettings.zoomLevel);

                SolidBrush brush = new SolidBrush(Color.Black);
                Pen pen = new Pen(brush);

                if (this.renderedImage == null)
                {
                    bool isv = true;
                    int mWidth = this.image.Mapper.GetXLength(isv);
                    int mHeight = isv ? this.image.Mapper.GetYLength(isv) : this.image.Mapper.GetZLength(isv);
                    int mDepth = isv ? this.image.Mapper.GetZLength(isv) : this.image.Mapper.GetYLength(isv);

                    Bitmap bm = new Bitmap(
                        width: mWidth * CalculatedTextureSize,
                        height: mHeight * CalculatedTextureSize,
                        format: PixelFormat.Format32bppArgb);

                    using (Graphics gImg = Graphics.FromImage(bm))
                    {
                        gImg.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                        gImg.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
                        gImg.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;

                        for (int x = 0; x < mWidth; x++)
                        {
                            worker.SafeReport(100 * x / mWidth, "Rendering to materials display...");
                            for (int y = 0; y < mHeight; y++)
                            {
                                for (int z = 0; z < mDepth; z++)
                                {
                                    if (isSelectiveLayerViewEnabled)
                                    {
                                        if (z != Options.Get.RenderedZIndexToShow)
                                        {
                                            continue;
                                        }
                                    }

                                    int xi = x * CalculatedTextureSize;
                                    int yi = y * CalculatedTextureSize;
                                    if (xi + MainForm.PanZoomSettings.zoomLevel >= 0 && yi + MainForm.PanZoomSettings.zoomLevel >= 0)
                                    {
                                        Material m = this.image.Mapper.GetMaterialAt(isv, x, y, z);

                                        if (m.BlockID != 0)
                                        {
                                            if (IsSolidColors)
                                            {
                                                brush.Color = this.image.GetColor(x, y);
                                                gImg.FillRectangle(brush, xi, yi, CalculatedTextureSize, CalculatedTextureSize);
                                            }
                                            else if (IsColorPalette)
                                            {
                                                brush.Color = this.image.GetColor(x, y);
                                                gImg.DrawImage(m.getImage(isSide), xi, yi, CalculatedTextureSize, CalculatedTextureSize);
                                                gImg.FillRectangle(brush, xi, yi, CalculatedTextureSize / 2, CalculatedTextureSize / 2);
                                                brush.Color = Color.Black;
                                                gImg.DrawRectangle(pen, xi, yi, CalculatedTextureSize / 2, CalculatedTextureSize / 2);
                                            }
                                            else
                                            {
                                                gImg.DrawImage(m.getImage(isSide), xi, yi, CalculatedTextureSize, CalculatedTextureSize);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    this.renderedImage = bm;
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            g.InterpolationMode = InterpolationMode.NearestNeighbor;
            g.SmoothingMode = SmoothingMode.None;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;

            if (this.image != null)
            {
                bool isSelectiveLayerViewEnabled = Options.Get.IsEnabled(Constants.RenderedZIndexFilter, false);

                bool isSide = Options.Get.IsSideView;
                double origW = this.image.Width;
                double origH = this.image.Height;
                int w = (int)(origW * MainForm.PanZoomSettings.zoomLevel);
                int h = (int)(origH * MainForm.PanZoomSettings.zoomLevel);
                int zoom = (int)(MainForm.PanZoomSettings.zoomLevel);

                if (MainForm.PanZoomSettings.zoomLevel < 1.0D)
                {
                    g.InterpolationMode = InterpolationMode.Bicubic;
                    g.CompositingQuality = CompositingQuality.Default;
                    g.SmoothingMode = SmoothingMode.HighSpeed;
                }
                else
                {
                    g.InterpolationMode = InterpolationMode.NearestNeighbor;
                    g.CompositingQuality = CompositingQuality.HighSpeed;
                    g.SmoothingMode = SmoothingMode.AntiAlias;
                }

                SolidBrush brush = new SolidBrush(Color.Black);
                Pen pen = new Pen(brush);

                if (this.renderedImage == null)
                {
                    bool isv = true;
                    int mWidth = this.image.Mapper.GetXLength(isv);
                    int mHeight = isv ? this.image.Mapper.GetYLength(isv) : this.image.Mapper.GetZLength(isv);
                    int mDepth = isv ? this.image.Mapper.GetZLength(isv) : this.image.Mapper.GetYLength(isv);

                    Bitmap bm = new Bitmap(
                        width: mWidth * CalculatedTextureSize,
                        height: mHeight * CalculatedTextureSize,
                        format: PixelFormat.Format32bppArgb);

                    this.renderedImage = bm;
                    using (Graphics gImg = Graphics.FromImage(bm))
                    {
                        gImg.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                        gImg.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
                        gImg.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;

                        for (int x = 0; x < mWidth; x++)
                        {
                            for (int y = 0; y < mHeight; y++)
                            {
                                for (int z = 0; z < mDepth; z++)
                                {
                                    if (isSelectiveLayerViewEnabled)
                                    {
                                        if (z != Options.Get.RenderedZIndexToShow)
                                        {
                                            continue;
                                        }
                                    }

                                    int xi = x * CalculatedTextureSize;
                                    int yi = y * CalculatedTextureSize;
                                    if (xi + MainForm.PanZoomSettings.zoomLevel >= 0 && yi + MainForm.PanZoomSettings.zoomLevel >= 0)
                                    {
                                        Material m = this.image.Mapper.GetMaterialAt(isv, x, y, z);

                                        if (m.BlockID != 0)
                                        {
                                            if (IsSolidColors)
                                            {
                                                brush.Color = this.image.GetColor(x, y);
                                                gImg.FillRectangle(brush, xi, yi, CalculatedTextureSize, CalculatedTextureSize);
                                            }
                                            else if (IsColorPalette)
                                            {
                                                brush.Color = this.image.GetColor(x, y);
                                                gImg.DrawImage(m.getImage(isSide), xi, yi, CalculatedTextureSize, CalculatedTextureSize);
                                                gImg.FillRectangle(brush, xi, yi, CalculatedTextureSize / 2, CalculatedTextureSize / 2);
                                                brush.Color = Color.Black;
                                                gImg.DrawRectangle(pen, xi, yi, CalculatedTextureSize / 2, CalculatedTextureSize / 2);
                                            }
                                            else
                                            {
                                                gImg.DrawImage(m.getImage(isSide), xi, yi, CalculatedTextureSize, CalculatedTextureSize);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //g = e.Graphics;

                Point pStart = getPointOnImage(new Point(0, 0), EstimateProp.Floor);
                Point fStart = getPointOnPanel(pStart);
                pStart.X *= CalculatedTextureSize;
                pStart.Y *= CalculatedTextureSize;

                Point pEnd = getPointOnImage(new Point(this.Width, this.Height), EstimateProp.Ceil);
                Point fEnd = getPointOnPanel(pEnd);
                pEnd.X *= CalculatedTextureSize;
                pEnd.Y *= CalculatedTextureSize;

                Rectangle rectSRC = new Rectangle(pStart, pStart.CalculateSize(pEnd));
                Rectangle rectDST = new Rectangle(fStart, fStart.CalculateSize(fEnd));


                try
                {
                    int numMegaBytes = 32 / 8 * CalculatedTextureSize * CalculatedTextureSize * image.Height * image.Width / 1024 / 1024;
                    using (var memoryCheck = new System.Runtime.MemoryFailPoint(numMegaBytes))
                    {
                        g.DrawImage(image: this.renderedImage,
                            srcRect: rectSRC,
                            destRect: rectDST,
                            srcUnit: GraphicsUnit.Pixel);
                    }
                }
                catch (InsufficientMemoryException ex)
                {
                    MainForm.Self.PreRenderedImage.DisposeSafely();
                    MainForm.Self.PreRenderedImage = null;
                    CalculatedTextureSize = Math.Max(1, CalculatedTextureSize - 2);
                    if (CalculatedTextureSize <= 2)
                    {
                        MessageBox.Show("Not enough memory. Please try again or reduce image size if problem continues.");
                    }
                    else
                    {
                        ForceReRender();
                    }
                    return;
                }



                if (this.IsShowBorder)
                {
                    Point pp2 = getPointOnPanel(new Point(0, 0));
                    using (Pen penWE = new Pen(Color.FromArgb(127, 0, 0, 0), zoom))
                    {
                        penWE.Alignment = PenAlignment.Inset;
                        g.DrawRectangle(penWE, pp2.X, pp2.Y, (int)(this.image.Width * MainForm.PanZoomSettings.zoomLevel), (int)(this.image.Height * MainForm.PanZoomSettings.zoomLevel));
                    }
                }

                if (Constants.IsFullVersion)
                {
                    brush.Color = Color.Red;
                    Point wePoint = getPointOnPanel(image.WorldEditOrigin);
                    g.FillRectangle(brush, wePoint.X, wePoint.Y, zoom + 1, zoom + 1);
                }

                if ((this.IsShowGrid) && (MainForm.PanZoomSettings.zoomLevel > 0.3D))
                {
                    drawGrid(g, Options.Get.GridSize, Color.Black);
                    if (MainForm.PanZoomSettings.zoomLevel > 5) drawGrid(g, 1, Color.FromArgb(40, 0, 0, 0));
                }
            }
        }

        #region Mouse Events
        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);
            if (e.Delta != 0)
            {
                Point panelPoint = e.Location;
                Point imagePoint = this.getPointOnImage(panelPoint, EstimateProp.Round);
                if (e.Delta < 0)
                {
                    MainForm.PanZoomSettings.zoomLevel *= 0.8;
                }
                else
                {
                    MainForm.PanZoomSettings.zoomLevel *= 1.2;
                }
                this.restrictZoom();
                MainForm.PanZoomSettings.imageX = ((int)Math.Round(panelPoint.X - imagePoint.X * MainForm.PanZoomSettings.zoomLevel));
                MainForm.PanZoomSettings.imageY = ((int)Math.Round(panelPoint.Y - imagePoint.Y * MainForm.PanZoomSettings.zoomLevel));
                this.Refresh();
            }
        }

        private void ImagePanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
            {
                this.initialDragPoint = e.Location;
                MainForm.PanZoomSettings.initialImageX = MainForm.PanZoomSettings.imageX;
                MainForm.PanZoomSettings.initialImageY = MainForm.PanZoomSettings.imageY;
                this.Cursor = new Cursor(Resources.cursor_handclosed.GetHicon());
                this.IsDragging = true;
            }
            else
            {
                RenderedImagePanel panel = (RenderedImagePanel)sender;
                Point loc = getPointOnImage(e.Location, EstimateProp.Floor);
                if (Options.Get.IsSideView)
                {
                    this.ts_xyz.Text = $"XYZ : ({loc.X}, {(this.image.Mapper.GetYLength(true) - loc.Y)}, *)";
                }
                else
                {
                    this.ts_xyz.Text = $"XYZ : ({loc.X}, *, {loc.Y})";
                }
                Material[] ms = this.image.Mapper.GetMaterialsAt(loc.X, loc.Y);
                this.ts_MaterialName.Text = $"Materials: \r\n{string.Join("\r\n", ms.Select(x => x.GetBlockNameAndData(true)))}";

                Color c = this.image.GetColor(loc.X, loc.Y);
                this.averageColorCodeToolStripMenuItem.Text = "AvgColor: #" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
                this.rGBAToolStripMenuItem.Text = $"RGBA: ({c.R}, {c.G}, {c.B}, {c.A})";
                this.contextMenu.Show(Cursor.Position);
            }
        }

        private void ImagePanel_MouseUp(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
            this.IsDragging = false;
        }

        private void ImagePanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsDragging)
            {
                //EditorPanel.this.fitToSize = false;
                Point point = e.Location;
                MainForm.PanZoomSettings.imageX = MainForm.PanZoomSettings.initialImageX - (this.initialDragPoint.X - point.X);
                MainForm.PanZoomSettings.imageY = MainForm.PanZoomSettings.initialImageY - (this.initialDragPoint.Y - point.Y);
                Refresh();
            }
        }
        #endregion 

        public Point getPointOnImage(Point pointOnPanel, EstimateProp prop)
        {
            if (prop == EstimateProp.Ceil)
            {
                return new Point((int)Math.Ceiling((pointOnPanel.X - MainForm.PanZoomSettings.imageX) / MainForm.PanZoomSettings.zoomLevel), (int)Math.Ceiling((pointOnPanel.Y - MainForm.PanZoomSettings.imageY) / MainForm.PanZoomSettings.zoomLevel));
            }
            if (prop == EstimateProp.Floor)
            {
                return new Point((int)Math.Floor((pointOnPanel.X - MainForm.PanZoomSettings.imageX) / MainForm.PanZoomSettings.zoomLevel), (int)Math.Floor((pointOnPanel.Y - MainForm.PanZoomSettings.imageY) / MainForm.PanZoomSettings.zoomLevel));
            }
            return new Point((int)Math.Round((pointOnPanel.X - MainForm.PanZoomSettings.imageX) / MainForm.PanZoomSettings.zoomLevel), (int)Math.Round((pointOnPanel.Y - MainForm.PanZoomSettings.imageY) / MainForm.PanZoomSettings.zoomLevel));
        }

        public Point getPointOnPanel(Point pointOnImage)
        {
            return new Point((int)Math.Round(pointOnImage.X * MainForm.PanZoomSettings.zoomLevel + MainForm.PanZoomSettings.imageX), (int)Math.Round(pointOnImage.Y * MainForm.PanZoomSettings.zoomLevel + MainForm.PanZoomSettings.imageY));
        }

        private void restrictZoom()
        {
            MainForm.PanZoomSettings.zoomLevel = (MainForm.PanZoomSettings.zoomLevel < MainForm.PanZoomSettings.minZoomLevel ? MainForm.PanZoomSettings.minZoomLevel : MainForm.PanZoomSettings.zoomLevel > MainForm.PanZoomSettings.maxZoomLevel ? MainForm.PanZoomSettings.maxZoomLevel : MainForm.PanZoomSettings.zoomLevel);
        }


        private void drawGrid(Graphics g, int gridSize, Color c)
        {
            int numHorizBlocks = (this.image.Width / gridSize);
            int numVertBlocks = (this.image.Height / gridSize);
            Pen p = new Pen(c);
            g.DrawLine(p, MainForm.PanZoomSettings.imageX, MainForm.PanZoomSettings.imageY, MainForm.PanZoomSettings.imageX, getRoundedZoomDistance(MainForm.PanZoomSettings.imageY, this.image.Height));
            g.DrawLine(p, MainForm.PanZoomSettings.imageX, getRoundedZoomDistance(MainForm.PanZoomSettings.imageY, this.image.Height), getRoundedZoomDistance(MainForm.PanZoomSettings.imageX, this.image.Width), getRoundedZoomDistance(MainForm.PanZoomSettings.imageY, this.image.Height));
            g.DrawLine(p, getRoundedZoomDistance(MainForm.PanZoomSettings.imageX, this.image.Width), getRoundedZoomDistance(MainForm.PanZoomSettings.imageY, this.image.Height), getRoundedZoomDistance(MainForm.PanZoomSettings.imageX, this.image.Width), MainForm.PanZoomSettings.imageY);
            g.DrawLine(p, getRoundedZoomDistance(MainForm.PanZoomSettings.imageX, this.image.Width), MainForm.PanZoomSettings.imageY, MainForm.PanZoomSettings.imageX, MainForm.PanZoomSettings.imageY);
            for (int x = 0; x < numHorizBlocks; x++)
            {
                g.DrawLine(p, getRoundedZoomX(x, gridSize), MainForm.PanZoomSettings.imageY, getRoundedZoomX(x, gridSize), getRoundedZoomDistance(MainForm.PanZoomSettings.imageY, this.image.Height));
            }
            for (int y = 0; y < numVertBlocks; y++)
            {
                g.DrawLine(p, MainForm.PanZoomSettings.imageX, getRoundedZoomY(y, gridSize), getRoundedZoomDistance(MainForm.PanZoomSettings.imageX, this.image.Width), getRoundedZoomY(y, gridSize));
            }
            p.Dispose();
        }

        private Point getShowingStart()
        {
            Point showingStart = getPointOnImage(new Point(0, 0), EstimateProp.Floor);
            showingStart.X = (showingStart.X < 0 ? 0 : showingStart.X);
            showingStart.Y = (showingStart.Y < 0 ? 0 : showingStart.Y);
            return showingStart;
        }

        private Point getShowingEnd(double origW, double origH)
        {
            Point showingEnd = getPointOnImage(new Point(Width, Height), EstimateProp.Ceil);
            showingEnd.X = (showingEnd.X > origW ? (int)origW : showingEnd.X);
            showingEnd.Y = (showingEnd.Y > origH ? (int)origH : showingEnd.Y);
            return showingEnd;
        }


        private int getRoundedZoomDistance(int x, int deltaX)
        {
            return (int)Math.Round(x + deltaX * MainForm.PanZoomSettings.zoomLevel);
        }

        private int getRoundedZoomX(int val, int blockSize)
        {
            return (int)Math.Floor(MainForm.PanZoomSettings.imageX + val * blockSize * MainForm.PanZoomSettings.zoomLevel);
        }

        private int getRoundedZoomY(int val, int blockSize)
        {
            return (int)Math.Floor(MainForm.PanZoomSettings.imageY + val * blockSize * MainForm.PanZoomSettings.zoomLevel);
        }

        public enum EstimateProp
        {
            Floor, Ceil, Round
        }

        private void RenderedImagePanel_DoubleClick(object sender, MouseEventArgs e)
        {
            if (Constants.IsFullVersion)
            {
                RenderedImagePanel panel = (RenderedImagePanel)sender;
                Point loc = getPointOnImage(e.Location, EstimateProp.Floor);
                image.WorldEditOrigin = loc;
                Refresh();
            }
        }
    }

}