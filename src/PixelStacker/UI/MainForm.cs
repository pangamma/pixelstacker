﻿using PixelStacker.Logic;
using PixelStacker.PreRender;
using PixelStacker.Properties;
using PixelStacker.UI;
using SimplePaletteQuantizer;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PixelStacker
{
    public partial class MainForm : Form
    {
        public static MainForm Self;
        public Bitmap LoadedImage { get; private set; } = Resources.colorwheel.To32bppBitmap();
        public Bitmap PreRenderedImage { get; set; } = null;
        public BlueprintPA LoadedBlueprint { get; private set; }
        private string loadedImageFilePath { get; set; }
        private MaterialOptionsWindow MaterialOptions { get; set; } = null;
        public static PanZoomSettings PanZoomSettings { get; set; } = null;
        public static ConcurrentDictionary<string, BackgroundWorker> Workers { get; set; } = new ConcurrentDictionary<string, BackgroundWorker>();

        public MainForm()
        {
            Self = this;
            InitializeComponent();
            this.imagePanelMain.SetImage(LoadedImage);
            this.Text = this.Text + " v" + Constants.Version;
            if (!Constants.IsFullVersion)
            {
                togglePaletteToolStripMenuItem.Visible = false;
                mi_PreRenderOptions.Visible = false;
                this.Text = this.Text + " (Free Version)";
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //this.imagePanelMain.SetImage(LoadedImage);
            //this.Text = this.Text + " v" + Constants.Version;
            //if (!Constants.IsFullVersion)
            //{
            //    togglePaletteToolStripMenuItem.Visible = false;
            //    mi_PreRenderOptions.Visible = false;
            //    this.Text = this.Text + " (Free Version)";
            //}
        }


        #region Workers
        public void CancelAllWorkers()
        {
            foreach (var worker in Workers)
            {
                if (worker.Value.WorkerSupportsCancellation)
                {
                    worker.Value.CancelAsync();
                }
            }

            Workers.Clear();
        }

        public static void CancelAndRemoveWorker(string key)
        {
            if (Workers.ContainsKey(key))
            {
                var oldWorker = Workers[key];
                if (oldWorker.WorkerSupportsCancellation && !oldWorker.CancellationPending)
                {
                    oldWorker.CancelAsync();
                }

                if (!Workers.TryRemove(key, out BackgroundWorker val))
                {

                }
            }
        }
        #endregion

        #region Events
        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.MaterialOptions == null)
            {
                this.MaterialOptions = new MaterialOptionsWindow(this);
            }

            if (!this.MaterialOptions.Visible)
            {
                this.MaterialOptions.Show(this);
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dlgOpen.ShowDialog();
        }

        private void dlgOpen_FileOk(object sender, CancelEventArgs e)
        {
            OpenFileDialog dialog = (OpenFileDialog)sender;
            this.loadedImageFilePath = dialog.FileName;
            this.reOpenToolStripMenuItem.Enabled = true;
            using (Bitmap img = (Bitmap)Bitmap.FromFile(this.loadedImageFilePath))
            {
                this.LoadedImage.DisposeSafely();
                this.LoadedImage = img.To32bppBitmap(); // creates a clone of the img, but in the 32bpp format.
            }

            MainForm.PanZoomSettings = null;
            this.imagePanelMain.SetImage(this.LoadedImage);
            this.PreRenderedImage.DisposeSafely();
            this.PreRenderedImage = null;
            ShowImagePanel();
        }
        
        public/* async*/ void PreRenderImage(bool clearCache = false)
        {
            if (clearCache)
            {
                this.PreRenderedImage.DisposeSafely();
                this.PreRenderedImage = null;
            }

            if (this.PreRenderedImage == null)
            {
                var engine = QuantizerEngine.Get;
                // Let's figure out sizing now.

                var LIM = this.LoadedImage;
                int mH = Options.Get.MaxHeight ?? LIM.Height;
                int mW = Options.Get.MaxWidth ?? LIM.Width;

                int H = LIM.Height;
                int W = LIM.Width;

                if (mW < mH)
                {
                    H = mW * H / W;
                    W = mW;
                }
                else
                {
                    W = mH * W / H;
                    H = mH;
                }

                var srcImage = this.LoadedImage.To32bppBitmap(W, H);

                Bitmap img = null;
                if (Options.Get.PreRender_IsEnabled)
                {
                    img = engine.RenderImage(srcImage).Result;
                    srcImage.DisposeSafely();
                }
                else
                {
                    // We can simplify this so that we cut total color counts down massively.
                    int factor = 5;
                    srcImage.ToEditStream(null, (int x, int y, Color c) => {
                        int a, r, b, g = 0;
                        a = (c.A < 32) ? 0 : 255;
                        r = ((c.R / factor) * factor);
                        g = ((c.G / factor) * factor);
                        b = ((c.B / factor) * factor);
                        return Color.FromArgb(a, r, g, b);
                    });

                    img = srcImage;
                    //img = srcImage.To32bppBitmap();
                    //srcImage.DisposeSafely();
                }

                // Resize based on new size
                {
                    var imgForSize = this.PreRenderedImage ?? this.LoadedImage;
                    if (W != imgForSize.Width || H != imgForSize.Height)
                    {
                        MainForm.PanZoomSettings = null;
                    }

                }

                this.PreRenderedImage = img;
                this.imagePanelMain.SetImage(PreRenderedImage);
                this.ShowImagePanel();
            }
        }

        private async void renderSchematicToolStripMenuItem_Click(object _sender, EventArgs _e)
        {
            CancelAndRemoveWorker("RenderSchematic");

            lblStatus.Text = "Optimizing...";
            PreRenderImage(false);

            if (this.PreRenderedImage != null)
            {
                if (this.PreRenderedImage.Width > 22000 / Constants.TextureSize || this.PreRenderedImage.Height > 22000 / Constants.TextureSize)
                {
                    MessageBox.Show("Your image is too big. Pick a smaller one.");
                    return;
                }

                if (this.PreRenderedImage.Height > 256 && Options.Get.IsSideView)
                {
                    MessageBox.Show("Minecraft cannot support images larger than 256 blocks in height.");
                    return;
                }
            }

            BackgroundWorker worker = new BackgroundWorker()
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true,
            };

            worker.ProgressChanged += (object sender, ProgressChangedEventArgs e) => {
                MainForm.Self.InvokeEx(x => {
                    if (e.UserState != null) { 
                        x.lblStatus.Text = e.UserState?.ToString() ?? "";
                    }
                    x.progressBar.Maximum = 100;
                    x.progressBar.Value = e.ProgressPercentage;
                });
            };

            BlueprintPA blueprint = null;
            worker.DoWork += async (object sender, DoWorkEventArgs e) => {
                var _worker = sender as BackgroundWorker;
                blueprint = await BlueprintPA.GetBluePrintAsync(_worker, this.PreRenderedImage);
                MainForm.Self.InvokeEx(x => {
                    x.LoadedBlueprint = blueprint;
                    x.ShowRenderedImagePanel();
                    x.renderedImagePanel.SetBluePrint(blueprint);
                    //x.renderedImagePanel.RenderBitmap(_worker);
                    //x.renderedImagePanel.Refresh();
                });
            };


            worker.RunWorkerCompleted += (object sender, RunWorkerCompletedEventArgs e) => {
                var _worker = sender as BackgroundWorker;
                MainForm.Self.InvokeEx(x => {
                    x.lblStatus.Text = "Finished.";
                    x.progressBar.Maximum = 100;
                    x.progressBar.Value = 0;
                });
            };

            Workers["RenderSchematic"] = worker;
            worker.RunWorkerAsync(this);
        }

        private void saveMenuClick(object sender, EventArgs e)
        {
            if (this.LoadedBlueprint != null)
            {
                dlgSave.ShowDialog();
            }
            else
            {
                this.exportSchematicToolStripMenuItem.Enabled = false;
            }
        }

        private void toggleGridToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.renderedImagePanel.IsShowGrid = !this.renderedImagePanel.IsShowGrid;
            Options.Save();
            Refresh();
        }

        private void toggleSolidColorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.renderedImagePanel.IsSolidColors = !this.renderedImagePanel.IsSolidColors;
            Options.Save();
            Refresh();
        }

        private void toggleBorderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.renderedImagePanel.IsShowBorder = !this.renderedImagePanel.IsShowBorder;
            Options.Save();
            Refresh();
        }

        private void togglePaletteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.renderedImagePanel.IsColorPalette = !this.renderedImagePanel.IsColorPalette;
            Options.Save();
            Refresh();
        }

        private void dlgSave_FileOk(object sender, CancelEventArgs e)
        {
            if (this.LoadedBlueprint != null)
            {
                SaveFileDialog dlg = (SaveFileDialog)sender;
                string fName = dlg.FileName;
                if (fName.ToLower().EndsWith(".schem"))
                {
                    SchemFormatter.writeBlueprint(fName, this.LoadedBlueprint);
                }
                else if (fName.ToLower().EndsWith(".schematic"))
                {
                    SchematicFormatter.writeBlueprint(fName, this.LoadedBlueprint);
                }
                else if (fName.ToLower().EndsWith(".png"))
                {
                    if (this.renderedImagePanel != null)
                    {
                        this.renderedImagePanel.SaveToPNG(fName);
                    }
                }
                else if (fName.ToLower().EndsWith(".csv"))
                {
                    Dictionary<Material, int> materialCounts = new Dictionary<Material, int>();
                    bool isv = Options.Get.IsSideView;
                    int xM = this.LoadedBlueprint.Mapper.GetXLength(isv);
                    int yM = this.LoadedBlueprint.Mapper.GetYLength(isv);
                    int zM = this.LoadedBlueprint.Mapper.GetZLength(isv);
                    for(int x = 0; x < xM; x++)
                    {
                        for(int y = 0; y < yM; y++)
                        {
                            for(int z = 0; z < zM; z++)
                            {
                                Material m = this.LoadedBlueprint.Mapper.GetMaterialAt(isv, x, y, z);
                                if (m != Materials.Air)
                                {
                                    if (!materialCounts.ContainsKey(m))
                                    {
                                        materialCounts.Add(m, 0);
                                    }

                                    materialCounts[m] = materialCounts[m] + 1;
                                }
                            }
                        }
                    }

                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("\"Material\",\"Block Count\"");
                    sb.AppendLine("\"Total\","+materialCounts.Values.Sum());
                    foreach (var kvp in materialCounts.OrderByDescending(x => x.Value))
                    {
                        sb.AppendLine($"\"{kvp.Key.GetBlockNameAndData(isv).Replace("\"","\"\"")}\",{kvp.Value}");
                    }
                    File.WriteAllText(fName, sb.ToString());
                }
            }
            else
            {
                this.exportSchematicToolStripMenuItem.Enabled = false;
            }
        }

        private void reOpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.loadedImageFilePath != null)
            {
                using (Bitmap img = (Bitmap)Bitmap.FromFile(this.loadedImageFilePath))
                {
                    this.LoadedImage.DisposeSafely();
                    this.LoadedImage = img.To32bppBitmap();
                }
                MainForm.PanZoomSettings = null;
                this.imagePanelMain.SetImage(this.LoadedImage);
                ShowImagePanel();
            }
        }
        #endregion

        #region Utility / Draw methods
        public void ShowImagePanel()
        {
            this.imagePanelMain.Show();
            this.renderedImagePanel.Hide();
            this.imagePanelMain.BringToFront();
            this.exportSchematicToolStripMenuItem.Enabled = false;
            this.toggleBorderToolStripMenuItem.Enabled = false;
            this.toggleGridToolStripMenuItem.Enabled = false;
            this.toggleSolidColorsToolStripMenuItem.Enabled = false;
            this.togglePaletteToolStripMenuItem.Enabled = false;
            this.toggleLayerFilterToolStripMenuItem.Enabled = false;
            this.up1LayerToolStripMenuItem.Enabled = false;
            this.down1LayerToolStripMenuItem.Enabled = false;
            this.layerFilteringToolStripMenuItem.Enabled = false;
        }

        public void ShowRenderedImagePanel()
        {
            this.renderedImagePanel.Show();
            this.renderedImagePanel.BringToFront();
            this.imagePanelMain.Hide();
            this.exportSchematicToolStripMenuItem.Enabled = true;
            this.toggleBorderToolStripMenuItem.Enabled = true;
            this.toggleGridToolStripMenuItem.Enabled = true;
            this.toggleSolidColorsToolStripMenuItem.Enabled = true;
            this.toggleLayerFilterToolStripMenuItem.Enabled = Constants.IsFullVersion;
            this.up1LayerToolStripMenuItem.Enabled = Constants.IsFullVersion;
            this.down1LayerToolStripMenuItem.Enabled = Constants.IsFullVersion;
            this.layerFilteringToolStripMenuItem.Enabled = Constants.IsFullVersion;
            this.togglePaletteToolStripMenuItem.Enabled = Constants.IsFullVersion;
        }
        
        private void drawGrid(Bitmap bm, Graphics g, int blockSize, Pen p)
        {
            int numHorizBlocks = (bm.Width / blockSize);
            int numVertBlocks = (bm.Height / blockSize);
            g.DrawLine(p, 0, 0, 0, bm.Height * Constants.TextureSize);
            g.DrawLine(p, 0, bm.Height*Constants.TextureSize, bm.Width*Constants.TextureSize, bm.Height*Constants.TextureSize);
            g.DrawLine(p, bm.Width*Constants.TextureSize, bm.Height*Constants.TextureSize, bm.Width*Constants.TextureSize, 0);
            g.DrawLine(p, bm.Width*Constants.TextureSize, 0, 0, 0);
            for (int x = 0; x < numHorizBlocks; x++)
            {
                g.DrawLine(p, x*blockSize*Constants.TextureSize, 0, x * blockSize * Constants.TextureSize, bm.Height*Constants.TextureSize);
            }
            for (int y = 0; y < numVertBlocks; y++)
            {
                g.DrawLine(p, 0, y * blockSize * Constants.TextureSize, bm.Width*Constants.TextureSize, y * blockSize * Constants.TextureSize);
            }
        }
        #endregion

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var about = new AboutForm();
            about.ShowDialog(this);
        }

        private void prerenderOptions_Click(object sender, EventArgs e)
        {
            var about = new PreRenderOptionsForm(this);
            about.Show();
        }

        private void mi_preRender_Click(object sender, EventArgs e)
        {
            this.PreRenderImage(true);
        }

        private void toggleProgressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool isShowing = this.progressBar.Visible == false;
            int deltaY = !isShowing ? this.progressBar.Height : -this.progressBar.Height;
            this.progressBar.Visible = isShowing;
            {
                var oSize = this.imagePanelMain.Size;
                var nSize = new Size(oSize.Width, oSize.Height + deltaY);
                this.imagePanelMain.Size = nSize;
            }
            {
                var oSize = this.renderedImagePanel.Size;
                var nSize = new Size(oSize.Width, oSize.Height + deltaY);
                this.renderedImagePanel.Size = nSize;
            }
        }

        private void toggleLayerFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool orig = Options.Get.IsEnabled(Constants.RenderedZIndexFilter, false);
            Options.Get.SetEnabled(Constants.RenderedZIndexFilter, !orig);
            this.up1LayerToolStripMenuItem.Enabled = !orig;
            this.down1LayerToolStripMenuItem.Enabled = !orig;
            this.renderedImagePanel.ForceReRender();
        }

        private void up1LayerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Options.Get.IsEnabled(Constants.RenderedZIndexFilter, false))
            {
                int nVal = Math.Max(Options.Get.RenderedZIndexToShow - 1, 0);
                Options.Get.RenderedZIndexToShow = nVal;
                this.renderedImagePanel.ForceReRender();
            }
        }

        private void down1LayerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Options.Get.IsEnabled(Constants.RenderedZIndexFilter, false))
            {
                int nVal = Math.Min((Options.Get.RenderedZIndexToShow) + 1, this.LoadedBlueprint.MaxDepth - 1);
                Options.Get.RenderedZIndexToShow = nVal;
                this.renderedImagePanel.ForceReRender();
            }
        }

        private void otherOptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var about = new OtherOptionsWindow();
            about.Show(this);
        }
    }
}
