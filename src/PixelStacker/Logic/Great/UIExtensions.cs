﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PixelStacker.Logic
{
    public static class UIExtensions
    {
        public static TResult InvokeEx<TControl, TResult>(this TControl control, Func<TControl, TResult> func) where TControl : Control
        {
            return control.InvokeRequired
                ? (TResult)control.Invoke(func, control)
                : func(control);
        }

        public static void InvokeEx<TControl>(this TControl control, Action<TControl> func) where TControl : Control
        {
            control.InvokeEx(c => { func(c); return c; });
        }

        public static void InvokeEx<TControl>(this TControl control, Action action) where TControl : Control
        {
            control.InvokeEx(c => action());
        }

        public static void SafeReport(this BackgroundWorker worker, int value, string message = null)
        {
            if (worker == null) return;

            if (worker.WorkerSupportsCancellation && worker.CancellationPending)
            {
                return;
            }

            if (worker.IsBusy)
            {
                if (worker.WorkerReportsProgress)
                {
                    if (message != null) worker.ReportProgress(value, message);
                    else worker.ReportProgress(value);
                }
            }
        }
    }
}
