﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelStacker.Logic.WIP
{
    public class BackgroundWorkerHelper
    {
        private static ConcurrentDictionary<string, BackgroundWorkerNode> Workers { get; set; } = new ConcurrentDictionary<string, BackgroundWorkerNode>();

        public static BackgroundWorker CreateWorker(string key, DoWorkEventHandler doWorkHandler, RunWorkerCompletedEventHandler completedHandler)
        {
            if (Workers.ContainsKey(key))
            {
                if (Workers.TryGetValue(key, out BackgroundWorkerNode node))
                {
                    node.CancelAsync();
                    if (Workers.TryRemove(key, out node))
                    {
                    }
                }
            }

            BackgroundWorkerNode worker = new BackgroundWorkerNode()
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true,
            };

            worker.ProgressChanged += (object sender, ProgressChangedEventArgs e) => {
                var _worker = sender as BackgroundWorkerNode;
                if (_worker.LastPercent != e.ProgressPercentage || (e.UserState != null && _worker.LastMessage != e.UserState?.ToString()))
                {
                    string msg = e.UserState?.ToString() ?? _worker.LastMessage;
                    int percent = e.ProgressPercentage;

                    MainForm.Self.InvokeEx(x => {
                        x.lblStatus.Text = msg;
                        x.progressBar.Maximum = 100;
                        x.progressBar.Value = percent;
                    });
                }
            };

            worker.DoWork += doWorkHandler;


            worker.RunWorkerCompleted += (object sender, RunWorkerCompletedEventArgs e) => {
                var _worker = sender as BackgroundWorkerNode;
            };

            Workers.TryAdd(key, worker);
            worker.RunWorkerAsync(MainForm.Self);
            return worker;
        }

        public static void CancelAllWorkers()
        {
            foreach (var worker in Workers)
            {
                if (worker.Value.WorkerSupportsCancellation)
                {
                    worker.Value.CancelAsync();
                }
            }

            Workers.Clear();
        }

        public static void CancelAndRemoveWorker(string key)
        {
            if (Workers.ContainsKey(key))
            {
                var oldWorker = Workers[key];
                if (oldWorker.WorkerSupportsCancellation && !oldWorker.CancellationPending)
                {
                    oldWorker.CancelAsync();
                }

                if (!Workers.TryRemove(key, out BackgroundWorkerNode val))
                {

                }
            }
        }

        public class BackgroundWorkerNode : BackgroundWorker
        {
            public bool IsFinished { get => this.IsBusy == false; }
            public string LastMessage { get; set; }
            public int LastPercent { get; set; }
        }
    }
}
